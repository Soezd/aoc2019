package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	f, err := os.Open("./input.txt")
	if err != nil {
		panic(err)
	}
	sum := 0
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			fmt.Errorf("Error converting %s to integer: %s", scanner.Text(), err)
		}
		sum += num/3 - 2
	}
	fmt.Println(sum)

}
