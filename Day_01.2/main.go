package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	f, err := os.Open("./input.txt")
	if err != nil {
		panic(err)
	}
	sum := 0
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			fmt.Errorf("Error converting %s to integer: %s", scanner.Text(), err)
		}
		fuel := num/3 - 2
		sum += fuel

		sum += fuelNeededForFuel(fuel)
	}
	fmt.Println(sum)

}

func fuelNeededForFuel(f int) int {
	if f < 9 {
		return 0
	}
	fuel := f/3 - 2
	return fuel + fuelNeededForFuel(fuel)
}
