package main

import (
	"reflect"
	"testing"
)

func Test_run(t *testing.T) {
	tests := []struct {
		name   string
		in     []int
		expect []int
	}{
		{
			name:   "test1",
			in:     []int{1, 0, 0, 0, 99},
			expect: []int{2, 0, 0, 0, 99},
		},
		{
			name:   "test2",
			in:     []int{2, 3, 0, 3, 99},
			expect: []int{2, 3, 0, 6, 99},
		},
		{
			name:   "test3",
			in:     []int{2, 4, 4, 5, 99, 0},
			expect: []int{2, 4, 4, 5, 99, 9801},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			run(tt.in)
			if !reflect.DeepEqual(tt.expect, tt.in) {
				t.Fail()
			}
		})
	}
}
