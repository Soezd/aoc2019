package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	f, err := os.Open("./input.txt")
	if err != nil {
		panic(err)
	}
	input, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	strCode := strings.Split(string(input), ",")
	intCode := make([]int, len(strCode))
	for k, v := range strCode {
		i, err := strconv.Atoi(v)
		if err != nil {
			_ = fmt.Errorf("could not cast string %s to int: %s", v, err)
			return
		}
		intCode[k] = i
	}

	intCode[1] = 12
	intCode[2] = 2

	run(intCode)
	printIntSlice(intCode)
}

func run(in []int) {
	var cmd, reg1, reg2 int
	for k, v := range in {
		if v == 99 && k%4 == 0 {
			return
		}

		switch k % 4 {
		case 0:
			cmd = v
		case 1:
			reg1 = v
		case 2:
			reg2 = v
		case 3:
			if cmd == 1 {
				in[v] = in[reg1] + in[reg2]
			} else {
				in[v] = in[reg1] * in[reg2]
			}
			cmd = 0
			reg1 = 0
			reg2 = 0
		}
	}
}

func printIntSlice(in []int) {
	for k, v := range in {
		fmt.Print(v)
		if k%4 == 3 {
			fmt.Print("\n")
		} else {
			fmt.Print(", ")
		}
	}
}
